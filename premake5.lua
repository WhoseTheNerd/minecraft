workspace "minecraft"
    architecture "x86_64"
    startproject "MinecraftApp"
    configurations
    {
        "Debug",
        "Release"
    }

    flags
	{
		"MultiProcessorCompile"
	}

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

IncludeDir = {}
IncludeDir["spdlog"] = "%{wks.location}/BlazeEngine/vendor/spdlog/include"
IncludeDir["glfw"] = "%{wks.location}/BlazeEngine/vendor/glfw/include"
IncludeDir["glad"] = "%{wks.location}/BlazeEngine/vendor/glad/include"
IncludeDir["glm"] = "%{wks.location}/BlazeEngine/vendor/glm/"
IncludeDir["tclap"] = "%{wks.location}/BlazeEngine/vendor/tclap/include"
IncludeDir["stb_image"] = "%{wks.location}/BlazeEngine/vendor/stb_image"
IncludeDir["stb_truetype"] = "%{wks.location}/BlazeEngine/vendor/stb_truetype"

group "Dependencies"
include "BlazeEngine/vendor/glfw"
include "BlazeEngine/vendor/glad"
group ""

include "BlazeEngine"
include "MinecraftApp"
