project "MinecraftApp"
    kind "ConsoleApp"
    language "C++"
    cppdialect "C++17"
    staticruntime "on"

    targetdir ("%{wks.location}/bin/" .. outputdir .. "/%{prj.name}")
	objdir ("%{wks.location}/bin-int/" .. outputdir .. "/%{prj.name}")

    files
	{
		"src/**.h",
		"src/**.hpp",
		"src/**.cpp"
	}

	defines
	{
	    "GLFW_INCLUDE_NONE"
	}

	includedirs
	{
	    "%{IncludeDir.spdlog}",
	    "%{IncludeDir.glm}",
	    "%{IncludeDir.glad}",
	    "%{IncludeDir.tclap}",
	    "%{wks.location}/BlazeEngine/src",
	    "%{wks.location}/BlazeEngine/vendor",
	    "%{IncludeDir.stb_truetype}"
	}

	links
	{
	    "BlazeEngine"
	}

    filter "configurations:Debug"
        defines "BZ_DEBUG"
		runtime "Debug"
		symbols "on"

	filter "configurations:Release"
	    defines "BZ_DEBUG"
		runtime "Release"
		optimize "on"
