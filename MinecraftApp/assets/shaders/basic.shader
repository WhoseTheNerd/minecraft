#shader vertex
#version 330 core

layout(location = 0) in vec3 a_Position;
layout(location = 1) in vec3 a_Color;
layout(location = 2) in vec2 a_UV;

out DATA
{
    vec4 color;
    vec2 UV;
} vs_out;

void main() {
    vs_out.color = vec4(a_Color, 1.0);
    vs_out.UV = a_UV;

    gl_Position = vec4(a_Position, 1.0);
}
#shader fragment
#version 330 core

out vec4 FragColor;

in DATA
{
    vec4 color;
    vec2 UV;
} fs_in;

uniform sampler2D u_Texture;

void main() {
    FragColor = texture(u_Texture, fs_in.UV);
}