#include "MinecraftLayer.hpp"
#include "stb_truetype.h"


#include <tclap/CmdLine.h>

namespace Minecraft {

    MinecraftLayer::MinecraftLayer() {
        ParseArguments();
    }

    void MinecraftLayer::OnAttach() {
        struct Vertex
        {
            glm::vec3 Position;
            glm::vec3 Color;
            glm::vec2 UV;
        };

#if 0
        std::array<Vertex, 4> vertices = {{
            {{ 0.5f,  0.5f, 0.0f},  {1.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},  // top right
            {{ 0.5f, -0.5f, 0.0f},  {0.0f, 1.0f, 0.0f}, {1.0f, 0.0f}},  // bottom right
            {{-0.5f, -0.5f, 0.0f},  {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f}},  // bottom left
            {{-0.5f,  0.5f, 0.0f},  {0.0f, 1.0f, 1.0f}, {0.0f, 1.0f}}   // top left
        }};
#else
        std::array<Vertex, 4> vertices = {{
            {{ 0.5f,  0.5f, 0.0f},  {1.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},  // top right
            {{ 0.5f, -0.5f, 0.0f},  {0.0f, 1.0f, 0.0f}, {1.0f, 1.0f}},  // bottom right
            {{-0.5f, -0.5f, 0.0f},  {0.0f, 0.0f, 1.0f}, {0.0f, 1.0f}},  // bottom left
            {{-0.5f,  0.5f, 0.0f},  {0.0f, 1.0f, 1.0f}, {0.0f, 0.0f}}   // top left
        }};
#endif

        std::array<u32, 6> indices = {
                0, 1, 3, 1, 2, 3
        };

        m_VertexBuffer = VertexBuffer::Create(vertices.size() * sizeof(float) * sizeof(Vertex), reinterpret_cast<float*>(vertices.data()));
        m_VertexBuffer->Bind();
        m_VertexBuffer->SetLayout({
            {ShaderDataType::Float3, "a_Position" },
            {ShaderDataType::Float3, "a_Color"},
            {ShaderDataType::Float2, "a_UV"}
        });

        m_IndexBuffer = IndexBuffer::Create(indices.size(), indices.data());
        m_IndexBuffer->Bind();

        m_VertexArray = VertexArray::Create();
        m_VertexArray->Bind();
        m_VertexArray->AddVertexBuffer(m_VertexBuffer);
        m_VertexArray->SetIndexBuffer(m_IndexBuffer);

        m_Shader = Shader::Create(m_AssetsPath + "/shaders/basic.shader");
        m_Shader->Bind();

        //m_Texture = Texture2D::Create(m_AssetsPath + "/textures/wall.jpg");
        //m_Texture->Bind(0);

        SetupFontRendering();
        m_FontAtlasTexture->Bind(0);
        m_Shader->SetInt("u_Texture", 0);

        RenderCommand::Init();
        RenderCommand::SetClearColor({0.2f, 0.3f, 0.8f, 1.0f});

    }

    void MinecraftLayer::OnDetach() {

    }

    void MinecraftLayer::OnUpdate(BlazeEngine::Timestep ts) {
        RenderCommand::Clear();
        RenderCommand::DrawIndexed(m_VertexArray);
    }

    void MinecraftLayer::ParseArguments() {
        using namespace TCLAP;

        CmdArguments args = Application::Get()->GetArguments();

        CmdLine cmd("Command description message", ' ', "0.9");

        ValueArg<std::string> assetPathArg("a", "assets-path", "Location of a assets folder", true, "assets/", "string");
        cmd.add(assetPathArg);

        cmd.parse(args.GetArgc(), args.GetArgv());

        m_AssetsPath = assetPathArg.getValue();
    }

    void MinecraftLayer::OnEvent(Event &e) {
        EventDispatcher dispatcher(e);
        dispatcher.Dispatch<WindowResizeEvent>(BZ_BIND_EVENT_FN(MinecraftLayer::OnWindowResize));
    }

    bool MinecraftLayer::OnWindowResize(WindowResizeEvent &e) {
        RenderCommand::SetViewport(0, 0, e.GetWidth(), e.GetHeight());
        return true;
    }

    void MinecraftLayer::SetupFontRendering() {

        std::vector<u8> fontData = FileUtils::ReadFile(m_AssetsPath + "/fonts/OpenSans-Regular.ttf");

        constexpr u32 ATLAS_WIDTH = 1024;
        constexpr u32 ATLAS_HEIGHT = 1024;
        constexpr u32 CHAR_COUNT = '~' - ' ';
        constexpr u32 FONT_SIZE = 40;
        constexpr u32 OVERSAMPLE_X = 2;
        constexpr u32 OVERSAMPLE_Y = 2;
        constexpr u32 FONT_FIRSTCHAR = ' ';

        u8* atlasData = new u8[ATLAS_WIDTH*ATLAS_HEIGHT];

        stbtt_packedchar* charInfo = new stbtt_packedchar[CHAR_COUNT];

        stbtt_pack_context context;
        int status = stbtt_PackBegin(&context, atlasData, ATLAS_WIDTH, ATLAS_HEIGHT, 0, 1, nullptr);
        BZ_ASSERT(status, "Failed to initialize font");

        stbtt_PackSetOversampling(&context, OVERSAMPLE_X, OVERSAMPLE_Y);
        status = stbtt_PackFontRange(&context, fontData.data(), 0, FONT_SIZE, FONT_FIRSTCHAR, CHAR_COUNT, charInfo);
        BZ_ASSERT(status, "Failed to pack font");
        stbtt_PackEnd(&context);

        m_FontAtlasTexture = Texture2D::Create(ATLAS_WIDTH, ATLAS_HEIGHT);
        m_FontAtlasTexture->SetData(atlasData, ATLAS_WIDTH*ATLAS_HEIGHT);
    }

}
