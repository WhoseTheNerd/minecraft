#pragma once

#include <BlazeEngine.h>

namespace Minecraft {

    using namespace BlazeEngine;
    using namespace BlazeEngine::Graphics;

    class MinecraftLayer : public BlazeEngine::Layer
    {
    public:
        MinecraftLayer();

        void OnAttach() override;
        void OnDetach() override;
        void OnUpdate(BlazeEngine::Timestep ts) override;
        void OnEvent(Event& e) override;
    private:
        void ParseArguments();
        void SetupFontRendering();

        bool OnWindowResize(WindowResizeEvent& e);
    private:
        Ref<VertexBuffer> m_VertexBuffer;
        Ref<IndexBuffer> m_IndexBuffer;
        Ref<VertexArray> m_VertexArray;
        Ref<Shader> m_Shader;
        Ref<Texture2D> m_Texture;
        Ref<Texture2D> m_FontAtlasTexture;

        std::string m_AssetsPath {};
    };
}