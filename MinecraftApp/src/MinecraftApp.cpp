#include <cstdio>

#include <BlazeEngine.h>
#include <BlazeEngine/Core/EntryPoint.hpp>

#include "MinecraftLayer.hpp"

class MinecraftApp : public BlazeEngine::Application
{
public:
    MinecraftApp(BlazeEngine::CmdArguments args)
        : Application(args)
    {
        PushLayer(new Minecraft::MinecraftLayer());
    }
};

BlazeEngine::Scope<BlazeEngine::Application> BlazeEngine::CreateApplication(BlazeEngine::CmdArguments args)
{
    return BlazeEngine::CreateScope<MinecraftApp>(args);
}