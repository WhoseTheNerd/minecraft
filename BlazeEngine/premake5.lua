project "BlazeEngine"
    kind "StaticLib"
    language "C++"
    cppdialect "C++17"
    staticruntime "on"

    targetdir ("%{wks.location}/bin/" .. outputdir .. "/%{prj.name}")
	objdir ("%{wks.location}/bin-int/" .. outputdir .. "/%{prj.name}")

    files
	{
		"src/**.h",
		"src/**.hpp",
		"src/**.cpp",
		"vendor/stb_image/stb_image.h",
		"vendor/stb_image/stb_image.c",
		"vendor/stb_truetype/stb_truetype.h",
		"vendor/stb_truetype/stb_truetype.c"
	}

	defines
	{
	    "GLFW_INCLUDE_NONE"
	}

	includedirs
	{
		"src",
	    "%{IncludeDir.spdlog}",
	    "%{IncludeDir.glfw}",
	    "%{IncludeDir.glad}",
	    "%{IncludeDir.glm}",
	    "%{IncludeDir.tclap}",
	    "%{IncludeDir.stb_image}",
	    "%{IncludeDir.stb_truetype}"
	}

	links
	{
	    "GLFW",
	    "Glad",
	    "pthread"
	}

    filter "configurations:Debug"
        defines "BZ_DEBUG"
		runtime "Debug"
		symbols "on"

	filter "configurations:Release"
	    defines "BZ_DEBUG"
		runtime "Release"
		optimize "on"
