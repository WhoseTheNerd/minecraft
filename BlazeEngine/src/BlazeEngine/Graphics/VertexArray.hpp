#pragma once

#include "BlazeEngine/Core/Types.hpp"
#include "BlazeEngine/Graphics/VertexBuffer.hpp"
#include "BlazeEngine/Graphics/IndexBuffer.hpp"

namespace BlazeEngine::Graphics {

    class VertexArray
    {
    public:
        virtual ~VertexArray() = default;

        virtual void Bind() const = 0;

        virtual void AddVertexBuffer(const Ref<VertexBuffer>& vertexBuffer) = 0;
        virtual void SetIndexBuffer(const Ref<IndexBuffer>& indexBuffer) = 0;

        [[nodiscard]] virtual const std::vector<Ref<VertexBuffer>>& GetVertexBuffers() const = 0;
        [[nodiscard]] virtual const Ref<IndexBuffer>& GetIndexBuffer() const = 0;

        [[nodiscard]] virtual u32 GetHandle() const = 0;

        static Ref<VertexArray> Create();
    };
}
