#pragma once

#include <vector>
#include "BlazeEngine/Core/Types.hpp"
#include "BlazeEngine/Core/Base.hpp"

namespace BlazeEngine::Graphics {

    class IndexBuffer
    {
    public:
        virtual ~IndexBuffer() = default;

        virtual void Bind() const = 0;
        [[nodiscard]] virtual u32 GetCount() const = 0;

        [[nodiscard]] virtual u32 GetHandle() const = 0;

        static Ref<IndexBuffer> Create(u32 count, u32* indices);
        static Ref<IndexBuffer> Create(const std::vector<u32>& indices);

    };
}
