#pragma once

#include "BlazeEngine/Core/Base.hpp"
#include "BlazeEngine/Core/Types.hpp"
#include "BlazeEngine/Events/Event.hpp"

#include <string>

namespace BlazeEngine::Graphics {

    class Window
    {
    public:
        using EventCallbackFn = std::function<void(Event&)>;

        virtual ~Window() = default;

        virtual void OnUpdate() = 0;

        [[nodiscard]] virtual u32 GetWidth() const = 0;
        [[nodiscard]] virtual u32 GetHeight() const = 0;

        virtual void SetEventCallback(const EventCallbackFn& callback) = 0;

        [[nodiscard]] virtual void* GetHandle() const = 0;

        static Scope<Window> Create(u32 width, u32 height, const std::string& title);

    };
}
