#include "RenderCommand.hpp"

namespace BlazeEngine::Graphics {

    Scope<RendererAPI> RenderCommand::s_RendererAPI = RendererAPI::Create();
}