#pragma once

#include "BlazeEngine/Core/Types.hpp"
#include "BlazeEngine/Graphics/VertexArray.hpp"

#include <glm/glm.hpp>

namespace BlazeEngine::Graphics {

    class RendererAPI
    {
    public:
        virtual ~RendererAPI() = default;

        virtual void Init() = 0;
        virtual void SetViewport(u32 x, u32 y, u32 width, u32 height) = 0;
        virtual void SetClearColor(const glm::vec4& color) = 0;
        virtual void Clear() = 0;

        virtual void DrawIndexed(const Ref<VertexArray>& vertexArray) = 0;

        static Scope<RendererAPI> Create();
    };
}