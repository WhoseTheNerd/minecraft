#pragma once

#include <string>
#include <glm/glm.hpp>

#include "BlazeEngine/Core/Base.hpp"
#include "BlazeEngine/Core/Log.hpp"
#include "BlazeEngine/Core/Types.hpp"

namespace BlazeEngine::Graphics {

    class Shader
    {
    public:
        virtual ~Shader() = default;

        virtual void Bind() const = 0;

        virtual void SetInt(const char* name, int value) = 0;
        virtual void SetIntArray(const char* name, int* values, uint32_t count) = 0;
        virtual void SetFloat(const char* name, float value) = 0;
        virtual void SetFloat2(const char* name, const glm::vec2& value) = 0;
        virtual void SetFloat3(const char* name, const glm::vec3& value) = 0;
        virtual void SetFloat4(const char* name, const glm::vec4& value) = 0;
        virtual void SetMat4(const char* name, const glm::mat4& matrix) = 0;

        [[nodiscard]] virtual u32 GetHandle() const = 0;

        static Ref<Shader> Create(const std::string& shaderFile);
        static Ref<Shader> Create(const std::string& vertex, const std::string& fragment);
    };
}
