#pragma once

#include "BlazeEngine/Core/Base.hpp"

namespace BlazeEngine::Graphics {

    class GraphicsContext
    {
    public:
        virtual ~GraphicsContext() = default;

        virtual void Init() = 0;
        virtual void SwapBuffers() = 0;

        static Scope<GraphicsContext> Create(void* windowHandle);
    };
}