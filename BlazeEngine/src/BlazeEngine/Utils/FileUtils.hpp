#pragma once

#include "BlazeEngine/Core/Base.hpp"

namespace BlazeEngine {

    class FileUtils
    {
    public:
        static std::vector<u8> ReadFile(const std::string& path);
    };
}