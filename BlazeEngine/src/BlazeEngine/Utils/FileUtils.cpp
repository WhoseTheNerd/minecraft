#include "FileUtils.hpp"

namespace BlazeEngine {


    std::vector<u8> FileUtils::ReadFile(const std::string &path) {

        FILE* file = fopen(path.c_str(), "r");

        u32 size;
        fseek(file, 0, SEEK_END);
        size = ftell(file);
        fseek(file, 0, SEEK_SET);

        std::vector<u8> result(size);
        fread(result.data(), sizeof(u8), size, file);
        fclose(file);

        return result;
    }
}