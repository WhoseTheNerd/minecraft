#pragma once

#include "BlazeEngine/Core/Base.hpp"
#include "BlazeEngine/Core/System.hpp"
#include <vector>

namespace BlazeEngine {

    class SystemManager
    {
    public:
        ~SystemManager();

        void Initialize();

        void AddSystem(const Ref<System>& system);
    private:
        std::vector<Ref<System>> m_Systems;
    };
}
