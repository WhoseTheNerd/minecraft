#pragma once

#include <iostream>
#include <random>
#include <type_traits>
#include <array>

namespace BlazeEngine::Utils {

    class Random
    {
    public:
        template<typename T, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
        static constexpr T generate(T min, T max) {

            if constexpr (std::is_integral_v<T>) {
                std::uniform_int_distribution<T> dist(min, max);
                return dist(s_RNG);
            } else if constexpr (std::is_floating_point_v<T>) {
                std::uniform_real_distribution<T> dist(min, max);
                return dist(s_RNG);
            }
        }

        template <typename T, size_t N, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
        static constexpr void generate(T min, T max, std::array<T, N>& array)
        {
            if constexpr (std::is_integral_v<T>) {
                std::uniform_int_distribution<T> dist(min, max);
                for (size_t i = 0; i < N; ++i) {
                    array[i] = dist(s_RNG);
                }
            }
            else if constexpr (std::is_floating_point_v<T>) {
                std::uniform_real_distribution<T> dist(min, max);
                for (size_t i = 0; i < N; ++i) {
                    array[i] = dist(s_RNG);
                }
            }
        }
    private:
        static std::random_device s_Device;
        static std::mt19937 s_RNG;
    };

}