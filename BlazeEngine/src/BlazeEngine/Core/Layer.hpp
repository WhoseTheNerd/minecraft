#pragma once

#include "BlazeEngine/Core/Base.hpp"
#include "BlazeEngine/Core/Timestep.hpp"
#include "BlazeEngine/Events/Event.hpp"

namespace BlazeEngine {

    class Layer
    {
    public:
        virtual ~Layer() = default;

        virtual void OnAttach() = 0;
        virtual void OnDetach() = 0;
        virtual void OnUpdate(Timestep ts) = 0;
        virtual void OnEvent(Event& e) = 0;
    };
}