#include "Random.hpp"

namespace BlazeEngine::Utils {

    std::random_device Random::s_Device;
    std::mt19937 Random::s_RNG(s_Device());

}