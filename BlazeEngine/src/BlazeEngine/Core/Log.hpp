#pragma once

#include "spdlog/spdlog.h"

namespace BlazeEngine {

    class Log
    {
    public:
        static void Init();

        inline static std::shared_ptr<spdlog::logger>& GetCoreLogger() { return s_CoreLogger; }
    private:
        static std::shared_ptr<spdlog::logger> s_CoreLogger;
    };

}

#define BZ_TRACE(...) ::BlazeEngine::Log::GetCoreLogger()->trace(__VA_ARGS__)
#define BZ_INFO(...) ::BlazeEngine::Log::GetCoreLogger()->info(__VA_ARGS__)
#define BZ_WARN(...) ::BlazeEngine::Log::GetCoreLogger()->warn(__VA_ARGS__)
#define BZ_ERROR(...) ::BlazeEngine::Log::GetCoreLogger()->error(__VA_ARGS__)
#define BZ_CRITICAL(...) ::BlazeEngine::Log::GetCoreLogger()->critical(__VA_ARGS__)
