#pragma once

namespace BlazeEngine {

    class CmdArguments
    {
    public:
        CmdArguments(int argc, char** argv)
            : m_Argc(argc), m_Argv(argv)
        {}

        [[nodiscard]] int GetArgc() const { return m_Argc; }
        [[nodiscard]] char** GetArgv() const { return m_Argv;}

        // TODO: Implement iterators and index operator

    private:
        int m_Argc;
        char** m_Argv;
    };
}