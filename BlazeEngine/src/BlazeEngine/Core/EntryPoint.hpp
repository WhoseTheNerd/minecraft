#pragma once

#include "BlazeEngine/Core/Base.hpp"
#include "BlazeEngine/Core/Log.hpp"
#include "BlazeEngine/Core/Application.hpp"
#include "BlazeEngine/Core/SystemManager.hpp"
#include "BlazeEngine/Core/Systems/GLFWSystem.hpp"
#include "BlazeEngine/Core/Systems/LogSystem.hpp"
#include "BlazeEngine/Core/CmdArguments.hpp"

#include <iostream>

extern BlazeEngine::Scope<BlazeEngine::Application> BlazeEngine::CreateApplication(CmdArguments args);

int main(int argc, char** argv)
{
    BlazeEngine::SystemManager manager;
    manager.AddSystem(BlazeEngine::CreateRef<BlazeEngine::LogSystem>());
    manager.AddSystem(BlazeEngine::CreateRef<BlazeEngine::GLFWSystem>());
    manager.Initialize();

    auto app = BlazeEngine::CreateApplication({argc, argv});
    app->Run();
}