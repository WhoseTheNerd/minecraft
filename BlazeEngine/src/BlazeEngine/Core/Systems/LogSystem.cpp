#include "LogSystem.hpp"

#include "BlazeEngine/Core/Log.hpp"

namespace BlazeEngine {

    void LogSystem::Initialize() {
        Log::Init();
    }

    void LogSystem::Deinitialize() {
        // Log already manages resources using RAII
    }
}