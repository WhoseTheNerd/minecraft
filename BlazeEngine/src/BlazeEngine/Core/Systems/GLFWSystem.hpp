#pragma once

#include "BlazeEngine/Core/System.hpp"

namespace BlazeEngine {

    class GLFWSystem : public System
    {
    public:
        void Initialize() override;
        void Deinitialize() override;
    };
}

