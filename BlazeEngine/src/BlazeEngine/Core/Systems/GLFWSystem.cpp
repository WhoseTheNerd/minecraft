#include "GLFWSystem.hpp"
#include "BlazeEngine/Core/Base.hpp"

#include "GLFW/glfw3.h"

namespace BlazeEngine {

    void GLFWSystem::Initialize() {
        int status = glfwInit();
        BZ_ASSERT(status, "Failed to initialize GLFW!");
    }

    void GLFWSystem::Deinitialize() {
        glfwTerminate();
    }
}