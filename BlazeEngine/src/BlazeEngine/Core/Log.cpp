#include "Log.hpp"

#include "spdlog/sinks/stdout_color_sinks.h"

namespace BlazeEngine {

    std::shared_ptr<spdlog::logger> Log::s_CoreLogger;

    void Log::Init()
    {
        spdlog::set_pattern("%^[%T] %n: %v%$");
        s_CoreLogger = spdlog::stdout_color_mt("BlazeEngine");
        s_CoreLogger->set_level(spdlog::level::trace);
    }
}