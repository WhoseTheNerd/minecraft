#pragma once

#include "BlazeEngine/Core/Base.hpp"
#include "BlazeEngine/Core/LayerStack.hpp"
#include "BlazeEngine/Core/CmdArguments.hpp"

#include "BlazeEngine/Events/Event.hpp"
#include "BlazeEngine/Events/ApplicationEvent.hpp"

#include "BlazeEngine/Graphics/Window.hpp"

int main(int argc, char** argv);

namespace BlazeEngine {

    class Application
    {
    public:
        Application(CmdArguments args);
        virtual ~Application() = default;

        void OnEvent(Event& e);

        void PushLayer(Layer* layer);
        void PushOverlay(Layer* layer);

        [[nodiscard]] CmdArguments GetArguments() const { return m_Arguments; }

        static Application* Get() { return s_Instance; }
    private:
        void Run();
        bool OnWindowClose(WindowCloseEvent& e);
    private:
        Scope<Graphics::Window> m_Window;
        LayerStack m_LayerStack;
        bool m_Running = true;
        float m_LastFrameTime = 0.0f;
        CmdArguments m_Arguments;
    private:
        static Application* s_Instance;
        friend int ::main(int argc, char** argv);
    };

    Scope<Application> CreateApplication(CmdArguments args);
}