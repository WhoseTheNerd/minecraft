#include "Application.hpp"

#include <GLFW/glfw3.h>

namespace BlazeEngine {

    Application* Application::s_Instance = nullptr;

    Application::Application(CmdArguments args)
        : m_Arguments(args)
    {
        BZ_ASSERT(!s_Instance, "Application already exists!");
        s_Instance = this;
        m_Window = Graphics::Window::Create(960, 540, "Minecraft");
        m_Window->SetEventCallback(BZ_BIND_EVENT_FN(Application::OnEvent));
    }

    void Application::Run() {
        while (m_Running) {

            auto time = static_cast<float>(glfwGetTime());
            Timestep timestep = time - m_LastFrameTime;
            m_LastFrameTime = time;

            for (Layer* layer : m_LayerStack) {
                layer->OnUpdate(timestep);
            }

            m_Window->OnUpdate();
        }
    }

    void Application::PushLayer(Layer *layer) {
        m_LayerStack.PushLayer(layer);
        layer->OnAttach();
    }

    void Application::PushOverlay(Layer *layer) {
        m_LayerStack.PushOverlay(layer);
        layer->OnAttach();
    }

    void Application::OnEvent(Event &e) {
        EventDispatcher dispatcher(e);
        dispatcher.Dispatch<WindowCloseEvent>(BZ_BIND_EVENT_FN(Application::OnWindowClose));

        for (auto it = m_LayerStack.rbegin(); it != m_LayerStack.rend(); ++it)
        {
            if (e.Handled)
                break;
            (*it)->OnEvent(e);
        }
    }

    bool Application::OnWindowClose(WindowCloseEvent &e) {
        m_Running = false;
        return true;
    }
}