#pragma once

namespace BlazeEngine {

    class System
    {
    public:
        virtual void Initialize() = 0;
        virtual void Deinitialize() = 0;
    };
}