#include "SystemManager.hpp"

namespace BlazeEngine {

    SystemManager::~SystemManager() {
        for (auto system : m_Systems) {
            system->Deinitialize();
        }
    }

    void SystemManager::Initialize()
    {
        for (auto system : m_Systems) {
            system->Initialize();
        }
    }

    void SystemManager::AddSystem(const Ref<System>& system) {
        m_Systems.push_back(system);
    }


}