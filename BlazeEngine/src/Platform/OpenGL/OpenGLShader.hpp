#pragma once

#include "BlazeEngine/Graphics/Shader.hpp"

namespace BlazeEngine::Graphics {

    class OpenGLShader : public Shader
    {
    public:
        OpenGLShader(const std::string& shaderFile);
        OpenGLShader(const std::string& vertex, const std::string& fragment);
        ~OpenGLShader() override;

        void Bind() const override;

        void SetInt(const char* name, int value) override;
        void SetIntArray(const char* name, int* values, uint32_t count) override;
        void SetFloat(const char* name, float value) override;
        void SetFloat2(const char* name, const glm::vec2& value) override;
        void SetFloat3(const char* name, const glm::vec3& value) override;
        void SetFloat4(const char* name, const glm::vec4& value) override;
        void SetMat4(const char* name, const glm::mat4& matrix) override;

        [[nodiscard]] u32 GetHandle() const override { return m_Handle; }
    private:
        [[nodiscard]] s32 GetUniformLocation(const char* name) const;
        [[nodiscard]] static u32 CreateProgram(const std::string& vertex, const std::string& fragment);
        [[nodiscard]] static u32 CompileShader(u32 type, const std::string& source);
        [[nodiscard]] static std::pair<std::string, std::string> ParseShader(const std::string& path);
    private:
        u32 m_Handle = 0;
    };
}

