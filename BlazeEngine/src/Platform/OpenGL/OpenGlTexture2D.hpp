#pragma once

#include "BlazeEngine/Graphics/Texture.hpp"

namespace BlazeEngine::Graphics {

    class OpenGLTexture2D : public Texture2D
    {
    public:
        OpenGLTexture2D(u32 width, u32 height);
        OpenGLTexture2D(const std::string& path);
        ~OpenGLTexture2D() override;

        [[nodiscard]] u32 GetWidth() const override { return m_Width; }
        [[nodiscard]] u32 GetHeight() const override { return m_Height; }
        [[nodiscard]] u32 GetBPP()  const override { return m_BPP; }
        [[nodiscard]] u32 GetHandle() const override { return m_Height; }

        void SetData(void* data, u32 size) override;

        void Bind(u32 slot = 0) override;
    private:
        u32 m_Handle;
        u32 m_Width;
        u32 m_Height;
        u32 m_BPP;
    };
}