#include "OpenGLShader.hpp"

#include <glad/glad.h>
#include <glm/gtc/type_ptr.hpp>

#include <string>
#include <sstream>
#include <fstream>

namespace BlazeEngine::Graphics {

    Ref<Shader> Shader::Create(const std::string& vertex, const std::string& fragment)
    {
        return CreateRef<OpenGLShader>(vertex, fragment);
    }

    Ref<Shader> Shader::Create(const std::string& shaderFile)
    {
        return CreateRef<OpenGLShader>(shaderFile);
    }

    OpenGLShader::OpenGLShader(const std::string& shaderFile)
    {
        auto[vertex, fragment] = ParseShader(shaderFile);
        m_Handle = CreateProgram(vertex, fragment);
    }

    OpenGLShader::OpenGLShader(const std::string &vertex, const std::string &fragment) {
        m_Handle = CreateProgram(vertex, fragment);
    }

    OpenGLShader::~OpenGLShader() {
        glDeleteProgram(m_Handle);
    }

    void OpenGLShader::Bind() const {
        glUseProgram(m_Handle);
    }

    void OpenGLShader::SetInt(const char *name, int value) {
        glProgramUniform1i(m_Handle, GetUniformLocation(name), value);
    }

    void OpenGLShader::SetIntArray(const char *name, int *values, uint32_t count) {
        glProgramUniform1iv(m_Handle, GetUniformLocation(name), static_cast<GLsizei>(count), values);
    }

    void OpenGLShader::SetFloat(const char *name, float value) {
        glProgramUniform1f(m_Handle, GetUniformLocation(name), value);
    }

    void OpenGLShader::SetFloat2(const char *name, const glm::vec2 &value) {
        glProgramUniform2f(m_Handle, GetUniformLocation(name), value.x, value.y);
    }

    void OpenGLShader::SetFloat3(const char *name, const glm::vec3 &value) {
        glProgramUniform3f(m_Handle, GetUniformLocation(name), value.x, value.y, value.z);
    }

    void OpenGLShader::SetFloat4(const char *name, const glm::vec4 &value) {
        glProgramUniform4f(m_Handle, GetUniformLocation(name), value.x, value.y, value.z, value.w);
    }

    void OpenGLShader::SetMat4(const char *name, const glm::mat4 &matrix) {
        glProgramUniformMatrix4fv(m_Handle, GetUniformLocation(name), 1, GL_FALSE, glm::value_ptr(matrix));
    }

    s32 OpenGLShader::GetUniformLocation(const char *name) const {
        return glGetUniformLocation(m_Handle, static_cast<const char*>(name));
    }

    u32 OpenGLShader::CreateProgram(const std::string &vertex, const std::string &fragment) {
        u32 program = glCreateProgram();
        u32 vs = CompileShader(GL_VERTEX_SHADER, vertex);
        u32 fs = CompileShader(GL_FRAGMENT_SHADER, fragment);

        glAttachShader(program, vs);
        glAttachShader(program, fs);
        glLinkProgram(program);
        glValidateProgram(program);

        int result {};
        glGetProgramiv(program, GL_LINK_STATUS, &result);
        if (result == GL_FALSE)
        {
            int length {};
            glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
            std::vector<char> message(length);
            glGetProgramInfoLog(program, length, &length, message.data());
            BZ_CRITICAL("Failed to link the program!\nError message:\n{0}", message.data());
            BZ_ASSERT(false, "Failed to link the program!");
        }

        glDetachShader(program, vs);
        glDetachShader(program, fs);

        glDeleteShader(vs);
        glDeleteShader(fs);

        return program;
    }

    u32 OpenGLShader::CompileShader(u32 type, const std::string &source) {
        u32 shader = glCreateShader(type);
        const char* src = source.c_str();
        glShaderSource(shader, 1, &src, nullptr);
        glCompileShader(shader);

        int result {};
        glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
        if (result == GL_FALSE)
        {
            int length{};
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
            std::vector<char> message(length);
            glGetShaderInfoLog(shader, length, &length, message.data());
            const char* shaderType = type == GL_VERTEX_SHADER ? "Vertex Shader" : "Fragment Shader";
            BZ_CRITICAL("Failed to compile {0}!\nError Message:\n{1}", shaderType, message.data());
            BZ_ASSERT(false, "Failed to compile the shader!");
        }

        return shader;
    }

    std::pair<std::string, std::string> OpenGLShader::ParseShader(const std::string &path) {
        std::ifstream file(path);
        BZ_ASSERT(file.is_open(), "Couldn't open the file!");
        enum class ShaderType
        {
            NONE=-1, VERTEX = 0, FRAGMENT = 1
        };

        ShaderType type = ShaderType::NONE;

        std::string line;
        std::stringstream ss[2];
        while (std::getline(file, line))
        {
            if (line.find("#shader") != std::string::npos)
            {
                if (line.find("vertex") != std::string::npos) {
                    type = ShaderType::VERTEX;
                } else if (line.find("fragment") != std::string::npos) {
                    type = ShaderType::FRAGMENT;
                }
            }
            else
            {
                ss[static_cast<int>(type)] << line << '\n';
            }
        }

        return {ss[0].str(), ss[1].str()};
    }
}
