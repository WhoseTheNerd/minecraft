#pragma once

#include "BlazeEngine/Graphics/RendererAPI.hpp"

namespace BlazeEngine::Graphics {

    class OpenGLRendererAPI : public RendererAPI
    {
    public:
        void Init() override;
        void SetViewport(u32 x, u32 y, u32 width, u32 height) override;
        void SetClearColor(const glm::vec4& color) override;
        void Clear() override;

        void DrawIndexed(const Ref<VertexArray>& vertexArray) override;
    };
}