#include "OpenGLGraphicsContext.hpp"

#include <GLFW/glfw3.h>
#include <glad/glad.h>

namespace BlazeEngine::Graphics {

    Scope<GraphicsContext> GraphicsContext::Create(void* windowHandle)
    {
        return CreateScope<OpenGLGraphicsContext>(windowHandle);
    }

    OpenGLGraphicsContext::OpenGLGraphicsContext(void *windowHandle)
        : m_Handle(reinterpret_cast<GLFWwindow*>(windowHandle))
    {

    }

    void OpenGLGraphicsContext::Init() {
        glfwMakeContextCurrent(m_Handle);

        int status = gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress));
        BZ_ASSERT(status, "Failed to initialize Glad!");

        BZ_INFO("OpenGL Info:");
        BZ_INFO("  Vendor: {0}",   reinterpret_cast<const char*>(glGetString(GL_VENDOR)));
        BZ_INFO("  Renderer: {0}", reinterpret_cast<const char*>(glGetString(GL_RENDERER)));
        BZ_INFO("  Version: {0}",  reinterpret_cast<const char*>(glGetString(GL_VERSION)));

        BZ_ASSERT(GLVersion.major > 4 || (GLVersion.major == 4 && GLVersion.minor >= 5), "Hazel requires at least OpenGL version 4.5!");
    }

    void OpenGLGraphicsContext::SwapBuffers() {
        glfwSwapBuffers(m_Handle);
    }
}