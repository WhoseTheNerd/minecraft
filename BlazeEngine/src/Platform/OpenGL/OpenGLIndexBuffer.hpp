#pragma once

#include "BlazeEngine/Graphics/IndexBuffer.hpp"

namespace BlazeEngine::Graphics {

    class OpenGLIndexBuffer : public IndexBuffer
    {
    public:
        OpenGLIndexBuffer(u32 count, u32* indices);
        explicit OpenGLIndexBuffer(const std::vector<u32>& indices);
        ~OpenGLIndexBuffer() override;

        void Bind() const override;
        [[nodiscard]] u32 GetCount() const override { return m_Count; }

        [[nodiscard]] u32 GetHandle() const override { return m_Handle; }
    private:
        u32 m_Handle = 0;
        u32 m_Count;
    };
}
