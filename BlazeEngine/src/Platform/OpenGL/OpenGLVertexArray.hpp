#pragma once

#include "BlazeEngine/Graphics/VertexArray.hpp"

namespace BlazeEngine::Graphics {

    class OpenGLVertexArray : public VertexArray
    {
    public:
        OpenGLVertexArray();
        ~OpenGLVertexArray() override;

        void Bind() const override;

        void AddVertexBuffer(const Ref<VertexBuffer>& vertexBuffer) override;
        void SetIndexBuffer(const Ref<IndexBuffer>& indexBuffer) override;

        [[nodiscard]] const std::vector<Ref<VertexBuffer>>& GetVertexBuffers() const override { return m_VertexBuffers; }
        [[nodiscard]] const Ref<IndexBuffer>& GetIndexBuffer() const override { return m_IndexBuffer; }

        [[nodiscard]] u32 GetHandle() const override { return m_Handle; }
    private:
        u32 m_Handle = 0;
        u32 m_VertexBufferIndex = 0;
        std::vector<Ref<VertexBuffer>> m_VertexBuffers {};
        Ref<IndexBuffer> m_IndexBuffer {};
    };
}
