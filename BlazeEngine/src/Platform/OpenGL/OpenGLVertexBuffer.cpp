#include "OpenGLVertexBuffer.hpp"

#include <glad/glad.h>

namespace BlazeEngine::Graphics {

    Ref<VertexBuffer> VertexBuffer::Create(u32 size, float* vertices)
    {
        return CreateRef<OpenGLVertexBuffer>(size, vertices);
    }

    Ref<VertexBuffer> VertexBuffer::Create(const std::vector<float>& vertices)
    {
        return CreateRef<OpenGLVertexBuffer>(vertices);
    }

    OpenGLVertexBuffer::OpenGLVertexBuffer(u32 size, float *vertices) {
        glCreateBuffers(1, &m_Handle);
        glNamedBufferData(m_Handle, static_cast<GLsizeiptr>(size), vertices, GL_STATIC_DRAW);
    }

    OpenGLVertexBuffer::OpenGLVertexBuffer(const std::vector<float> &vertices) {
        glCreateBuffers(1, &m_Handle);
        glNamedBufferData(m_Handle, static_cast<GLsizeiptr>(vertices.size() * sizeof(float)), vertices.data(), GL_STATIC_DRAW);
    }

    OpenGLVertexBuffer::~OpenGLVertexBuffer() {
        glDeleteBuffers(1, &m_Handle);
    }

    void OpenGLVertexBuffer::Bind() const {
        glBindBuffer(GL_ARRAY_BUFFER, m_Handle);
    }
}