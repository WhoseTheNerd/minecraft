#pragma once

#include "BlazeEngine/Graphics/GraphicsContext.hpp"

struct GLFWwindow;

namespace BlazeEngine::Graphics {

    class OpenGLGraphicsContext : public GraphicsContext
    {
    public:
        explicit OpenGLGraphicsContext(void* windowHandle);

        void Init() override;
        void SwapBuffers() override;
    private:
        GLFWwindow* m_Handle;
    };
}