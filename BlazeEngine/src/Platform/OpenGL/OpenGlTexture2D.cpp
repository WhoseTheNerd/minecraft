#include "OpenGlTexture2D.hpp"

#include <glad/glad.h>
#include <stb_image.h>

namespace BlazeEngine::Graphics {

    Ref<Texture2D> Texture2D::Create(const std::string& path)
    {
        return CreateRef<OpenGLTexture2D>(path);
    }

    Ref<Texture2D> Texture2D::Create(u32 width, u32 height)
    {
        return CreateRef<OpenGLTexture2D>(width, height);
    }

    OpenGLTexture2D::OpenGLTexture2D(u32 width, u32 height)
        : m_Width(width), m_Height(height), m_BPP(3)
    {
        glCreateTextures(GL_TEXTURE_2D, 1, &m_Handle);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glTextureStorage2D(m_Handle, 1, GL_RGB8, static_cast<GLsizei>(m_Width), static_cast<GLsizei>(m_Height));
    }

    OpenGLTexture2D::OpenGLTexture2D(const std::string &path) {

        u8* data = static_cast<u8*>(stbi_load(path.c_str(),
                                              reinterpret_cast<int*>(&m_Width),
                                              reinterpret_cast<int*>(&m_Height),
                                              reinterpret_cast<int*>(&m_BPP), 4));

        glCreateTextures(GL_TEXTURE_2D, 1, &m_Handle);
        glTextureParameteri(m_Handle, GL_TEXTURE_WRAP_S, GL_NEAREST);
        glTextureParameteri(m_Handle, GL_TEXTURE_WRAP_T, GL_NEAREST);
        glTextureParameteri(m_Handle, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTextureParameteri(m_Handle, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glTextureStorage2D(m_Handle, 1, GL_RGBA8, static_cast<GLsizei>(m_Width), static_cast<GLsizei>(m_Height));
        glTextureSubImage2D(m_Handle, 0, 0, 0, static_cast<GLsizei>(m_Width), static_cast<GLsizei>(m_Height), GL_RGBA, GL_UNSIGNED_BYTE, data);

        glGenerateTextureMipmap(m_Handle);

        stbi_image_free(data);
    }

    OpenGLTexture2D::~OpenGLTexture2D() {
        glDeleteTextures(1, &m_Handle);
    }

    void OpenGLTexture2D::Bind(u32 slot) {
        glBindTextureUnit(slot, m_Handle);
    }

    void OpenGLTexture2D::SetData(void *data, u32 size) {
        glTextureSubImage2D(m_Handle, 0, 0, 0, static_cast<GLsizei>(m_Width), static_cast<GLsizei>(m_Height), GL_RED, GL_UNSIGNED_BYTE, data);
    }


}