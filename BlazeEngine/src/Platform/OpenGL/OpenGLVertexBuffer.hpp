#pragma once

#include "BlazeEngine/Graphics/VertexBuffer.hpp"

namespace BlazeEngine::Graphics {

    class OpenGLVertexBuffer : public VertexBuffer
    {
    public:
        OpenGLVertexBuffer(u32 size, float* vertices);
        explicit OpenGLVertexBuffer(const std::vector<float>& vertices);
        ~OpenGLVertexBuffer() override;

        void Bind() const override;

        [[nodiscard]] const BufferLayout& GetLayout() const override { return m_Layout; }
        void SetLayout(const BufferLayout& layout) override { m_Layout = layout; }

        [[nodiscard]] u32 GetHandle() const override { return m_Handle; }
    private:
        u32 m_Handle = 0;
        BufferLayout m_Layout;
    };

}
