#include "OpenGLIndexBuffer.hpp"

#include <glad/glad.h>

namespace BlazeEngine::Graphics {

    Ref<IndexBuffer> IndexBuffer::Create(u32 count, u32* indices)
    {
        return CreateRef<OpenGLIndexBuffer>(count, indices);
    }

    Ref<IndexBuffer> IndexBuffer::Create(const std::vector<u32>& indices)
    {
        return CreateRef<OpenGLIndexBuffer>(indices);
    }

    OpenGLIndexBuffer::OpenGLIndexBuffer(u32 count, u32 *indices)
        : m_Count(count)
    {
        glCreateBuffers(1, &m_Handle);
        glNamedBufferData(m_Handle, static_cast<GLsizeiptr>(count * sizeof(u32)), indices, GL_STATIC_DRAW);
    }

    OpenGLIndexBuffer::OpenGLIndexBuffer(const std::vector<u32> &indices)
        : m_Count(indices.size())
    {
        glCreateBuffers(1, &m_Handle);
        glNamedBufferData(m_Handle, static_cast<GLsizeiptr>(indices.size() * sizeof(u32)), indices.data(), GL_STATIC_DRAW);
    }

    OpenGLIndexBuffer::~OpenGLIndexBuffer() {
        glDeleteBuffers(1, &m_Handle);
    }

    void OpenGLIndexBuffer::Bind() const {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_Handle);
    }
}