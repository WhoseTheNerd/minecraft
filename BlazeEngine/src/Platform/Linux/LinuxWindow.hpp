#pragma once

#include "BlazeEngine/Graphics/Window.hpp"
#include "BlazeEngine/Graphics/GraphicsContext.hpp"

struct GLFWwindow;

namespace BlazeEngine::Graphics {

    class LinuxWindow : public Window
    {
    public:
        LinuxWindow(u32 width, u32 height, const std::string& title);
        ~LinuxWindow() override;

        void OnUpdate() override;

        [[nodiscard]] u32 GetWidth() const override { return m_Data.Width; }
        [[nodiscard]] u32 GetHeight() const override {return m_Data.Height; }

        void SetEventCallback(const EventCallbackFn& callback) override { m_Data.EventCallback = callback; }

        [[nodiscard]] void* GetHandle() const override { return m_Handle; }
    private:
        struct WindowData
        {
            u32 Width;
            u32 Height;
            std::string Title;

            EventCallbackFn EventCallback;
        };

        WindowData m_Data;
        GLFWwindow* m_Handle;
        Scope<GraphicsContext> m_Context;
    };
}
