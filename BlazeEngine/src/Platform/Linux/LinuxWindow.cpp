#include "LinuxWindow.hpp"

#include "BlazeEngine/Events/ApplicationEvent.hpp"
#include "BlazeEngine/Events/KeyEvent.hpp"
#include "BlazeEngine/Events/MouseEvent.hpp"

#include <GLFW/glfw3.h>
#include <glad/glad.h>

namespace BlazeEngine::Graphics {

    Scope<Window> Window::Create(u32 width, u32 height, const std::string& title)
    {
        return CreateScope<LinuxWindow>(width, height, title);
    }


    LinuxWindow::LinuxWindow(u32 width, u32 height, const std::string &title)
        : m_Data{width, height, title}
    {
#if BZ_DEBUG
        glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
#endif
        m_Handle = glfwCreateWindow(static_cast<int>(width), static_cast<int>(height), title.c_str(), nullptr, nullptr);

        m_Context = GraphicsContext::Create(m_Handle);
        m_Context->Init();

        glfwSetWindowUserPointer(m_Handle, &m_Data);

        // Set GLFW callbacks
        glfwSetWindowSizeCallback(m_Handle, [](GLFWwindow* window, int width, int height)
        {
            auto* data = reinterpret_cast<WindowData*>(glfwGetWindowUserPointer(window));
            data->Width = width;
            data->Height = height;

            WindowResizeEvent event(width, height);
            data->EventCallback(event);
        });

        glfwSetWindowCloseCallback(m_Handle, [](GLFWwindow* window)
        {
            auto* data = reinterpret_cast<WindowData*>(glfwGetWindowUserPointer(window));
            WindowCloseEvent event;
            data->EventCallback(event);
        });

        glfwSetKeyCallback(m_Handle, [](GLFWwindow* window, int key, int scancode, int action, int mods)
        {
            auto* data = reinterpret_cast<WindowData*>(glfwGetWindowUserPointer(window));

            switch (action)
            {
                case GLFW_PRESS:
                {
                    KeyPressedEvent event(key, 0);
                    data->EventCallback(event);
                    break;
                }
                case GLFW_RELEASE:
                {
                    KeyReleasedEvent event(key);
                    data->EventCallback(event);
                    break;
                }
                case GLFW_REPEAT:
                {
                    KeyPressedEvent event(key, 1);
                    data->EventCallback(event);
                    break;
                }
            }
        });

        glfwSetCharCallback(m_Handle, [](GLFWwindow* window, unsigned int keycode)
        {
            auto* data = reinterpret_cast<WindowData*>(glfwGetWindowUserPointer(window));

            KeyTypedEvent event(keycode);
            data->EventCallback(event);
        });

        glfwSetMouseButtonCallback(m_Handle, [](GLFWwindow* window, int button, int action, int mods)
        {
            auto* data = reinterpret_cast<WindowData*>(glfwGetWindowUserPointer(window));

            switch (action)
            {
                case GLFW_PRESS:
                {
                    MouseButtonPressedEvent event(button);
                    data->EventCallback(event);
                    break;
                }
                case GLFW_RELEASE:
                {
                    MouseButtonReleasedEvent event(button);
                    data->EventCallback(event);
                    break;
                }
            }
        });

        glfwSetScrollCallback(m_Handle, [](GLFWwindow* window, double xOffset, double yOffset)
        {
            auto* data = reinterpret_cast<WindowData*>(glfwGetWindowUserPointer(window));

            MouseScrolledEvent event((float)xOffset, (float)yOffset);
            data->EventCallback(event);
        });

        glfwSetCursorPosCallback(m_Handle, [](GLFWwindow* window, double xPos, double yPos)
        {
            auto* data = reinterpret_cast<WindowData*>(glfwGetWindowUserPointer(window));

            MouseMovedEvent event((float)xPos, (float)yPos);
            data->EventCallback(event);
        });
    }

    LinuxWindow::~LinuxWindow() {
        glfwDestroyWindow(m_Handle);
    }

    void LinuxWindow::OnUpdate() {
        glfwPollEvents();
        m_Context->SwapBuffers();
    }
}