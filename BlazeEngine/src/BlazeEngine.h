#pragma once

#include "BlazeEngine/Core/Base.hpp"
#include "BlazeEngine/Core/Application.hpp"
#include "BlazeEngine/Core/Layer.hpp"
#include "BlazeEngine/Core/CmdArguments.hpp"

#include "BlazeEngine/Events/Event.hpp"
#include "BlazeEngine/Events/KeyEvent.hpp"
#include "BlazeEngine/Events/MouseEvent.hpp"
#include "BlazeEngine/Events/ApplicationEvent.hpp"

#include "BlazeEngine/Graphics/Window.hpp"
#include "BlazeEngine/Graphics/VertexBuffer.hpp"
#include "BlazeEngine/Graphics/IndexBuffer.hpp"
#include "BlazeEngine/Graphics/VertexArray.hpp"
#include "BlazeEngine/Graphics/Shader.hpp"
#include "BlazeEngine/Graphics/RenderCommand.hpp"
#include "BlazeEngine/Graphics/Texture.hpp"

#include "BlazeEngine/Utils/FileUtils.hpp"